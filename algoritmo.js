function findIndex(targetArray, target) {
  let output = [];
  for (let i = 0; i < targetArray.length; i++) {
    for (let j = i + 1; j < targetArray.length; j++) {
      const first = targetArray[i];
      const second = targetArray[j];
      const sum = first + second;
      if (sum === target) {
        output.push(i);
        output.push(j);
        break;
      }
    }
  }
  console.log(output);
  return output;
}

findIndex([2, 7, 11, 15], 9);
findIndex([3, 2, 4], 6);
findIndex([3, 3], 6);
findIndex([0, 3, 4, 7, 5], 8);
