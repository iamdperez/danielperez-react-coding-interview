import { Box, Button, TextField } from '@mui/material';
import { useState } from 'react';

export interface IInlineTextFieldProps {
  defaultValue: string;
  isValid?: (regex: string) => boolean;
  //   onSave: (value: string) => void;
}

const InlineTextField: React.FC<IInlineTextFieldProps> = ({
  defaultValue,
  //   onSave,
}) => {
  const [toolsVisible, setToolsVisible] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [value, setValue] = useState<string>(defaultValue);
  const handleClick = () => {
    setToolsVisible(true);
  };

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };
  const Tools = (
    <>
      <Button
        onClick={() => {
          setToolsVisible(false);
        }}
      >
        Save
      </Button>
      <Button
        onClick={() => {
          setValue(defaultValue);
          setToolsVisible(false);
        }}
      >
        Cancel
      </Button>
    </>
  );

  return (
    <div>
      <TextField
        variant="outlined"
        value={value}
        onClick={handleClick}
        onChange={handleOnChange}
        error={error}
      />
      {toolsVisible && Tools}
    </div>
  );
};

export default InlineTextField;
